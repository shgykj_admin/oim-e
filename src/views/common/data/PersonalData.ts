export default class PersonalData {
    public key: string = '';
    public name: string = '';
    public avatar: string = 'assets/images/common/head/user/1.png';
}
